//
//  baiduMapSearch.h
//  baiduMap
//
//  Created by LiangQiangkun on 16/5/20.
//
//

#import <Cordova/CDVPlugin.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>
#import "baiduMapViewController.h"
#import "UIImage+Rotate.h"
@interface baiduMapSearch : CDVPlugin <BMKMapViewDelegate,BMKRouteSearchDelegate,BMKBusLineSearchDelegate,BMKPoiSearchDelegate>
- (void)searchRoute:(CDVInvokedUrlCommand *)command;
- (void)drawRoute:(CDVInvokedUrlCommand *)command;
- (void)removeRoute:(CDVInvokedUrlCommand *)command;
- (void)searchBusRoute:(CDVInvokedUrlCommand *)command;
- (void)drawBusRoute:(CDVInvokedUrlCommand *)command;
// - (void)removeBusRoute:(CDVInvokedUrlCommand *)command;
- (void)searchInCity:(CDVInvokedUrlCommand *)command;
- (void)searchNearby:(CDVInvokedUrlCommand *)command;

@end
