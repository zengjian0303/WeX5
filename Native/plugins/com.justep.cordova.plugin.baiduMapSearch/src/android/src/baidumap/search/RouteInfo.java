package baidumap.search;


import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.overlayutil.OverlayManager;
import java.io.Serializable;
import java.util.List;

/**
 * Created by spark on 16/8/2.
 */
public class RouteInfo implements Serializable {
    public RouteInfo() {
    }

    private String id;
    private OverlayManager routeOverlay;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }


    public OverlayManager getRouteOverlay() {
        return routeOverlay;
    }

    public void setRouteOverlay(OverlayManager routeOverlay) {
        this.routeOverlay = routeOverlay;
    }
}