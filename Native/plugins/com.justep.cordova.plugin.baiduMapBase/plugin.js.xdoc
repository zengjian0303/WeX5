/**
	@name com.justep.cordova.plugin.baiduMapBase
	@class
	@description 百度地图插件基础版本
	@model Native
	@category plugin
	@declareVar navigator.baiduMap.base
*/
/**
   @name com.justep.cordova.plugin.baiduMapBase.annotationColor
   @property
   @type Number
   @description 大头针标注的颜色
*/
/**
   @name com.justep.cordova.plugin.baiduMapBase.baiduMapEvent
   @property
   @type String
   @description 地图相关事件的名称
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.open
 	@function
 	@param {Object} args
 	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"position":Object - 地图的位置及尺寸,例如：{x:0, y:0, w:320, h:480},缺省全屏显示,
 	 		"center":Object - 地图的中心位置,例如：{lon:116.397, lat:39.910},缺省北京天安门
 	 		"zoomLevel":Number - 缩放等级，取值范围3-21，缺省10 ,
 	 		"events":Object - 可选参数，要监听的事件以及回调,如{click:func, dbClick:func, longPress:func, viewChange:func},
 	 		"model":Object - 可选参数，当前model。如果传入model，则会在model onInActive时判地图是否已经被销毁，如果没有则会自动调用close销毁地图
		}
	</xmp>
 	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {void}
 	@description 打开百度地图
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.close
 	@function
 	@returns {void}
 	@description 关闭百度地图
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.removeEventListener
 	@function
 	@param {String} args 地图相关事件名称,取值范围(click/dbClick/longPress/viewChange)
 	@returns {void}
 	@description 停止监听地图相关事件
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setPosition
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"x":Number - 地图左上角的 x 坐标,
 	 		"y":Number - 地图左上角的 y 坐标,
 	 		"w":Number - 地图的宽度,
 	 		"h":Number - 地图的高度,
		}
	</xmp>
 	@returns {void}
 	@description 重新设置百度地图的显示位置及大小
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getCurrentLocation
	@function
	@param {Function} success 成功回调,返回经纬度与时间戳，返回示例:{lon:xx,lat:xx,timestamp:xx}
 	@param {Function} error 失败回调
 	@returns {Object}
 	@description 获取当前位置的经纬度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getLocationFromName
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"city":String - 地址所在城市,
 	 		"address":String - 地址信息,
		}
	</xmp>
	@param {Function} success 成功返回示例：{"lat":39.9151754663074,"lon":116.4039058301959}
 	@param {Function} error 失败回调
 	@returns {Object}
 	@description 根据地址信息获取经纬度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getNameFromLocation
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"lon":Number - 经度,
 	 		"lat":Number - 纬度,
		}
	</xmp>
	@param {Function} success 成功返回示例：{"status":0,"result":{"location":{"lng":116.3969999999999,"lat":39.91000007743232},"pois":[],"poiRegions":[{"direction_desc":"内","name":"国家大剧院","tag":"休闲娱乐"}],"sematic_description":"国家大剧院内","cityCode":131,"addressComponent":{"adcode":"110102","city":"北京市","country":"中国","distance":"66","district":"西城区","street":"前后井胡同","street_number":"11号","country_code":0,"direction":"南","province":"北京市"},"business":"天安门,和平门,前门","formatted_address":"北京市西城区前后井胡同11号"}}
 	@param {Function} error 失败回调
 	@returns {Object}
 	@description 根据经纬度查找地址信息
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.showCurrentLocation
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"isShow":Boolean - 是否在地图上显示用户位置,
 	 		"trackingMode":String - 当前位置显示形式,可选值为none(普通定位模式)/follow(定位跟随模式)/compass(定位罗盘模式),
		}
	</xmp>
 	@returns {void}
 	@description 是否在地图上显示当前位置，并且设置显示的样式
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setCenter
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"lon":Number - 经度,
 	 		"lat":Number - 纬度,
		}
	</xmp>
 	@returns {void}
 	@description 根据经纬度设置百度地图中心点
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getCenter
	@function
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {Object}中心点的经纬度，返回示例:{lon:xx,lat:xx};
 	@description 获取百度地图中心点的经纬度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setZoomLevel
	@function
	@param {Number} args 缩放等级	 取值范围：3-21级
 	@returns {void}
 	@description 设置百度地图缩放等级，此接口自带动画效果
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setMapAttr
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"type":String - 设置地图类型,none(空白地图)/standard(标准地图)/satellite(卫星地图)
 	 		"zoomEnable":Boolean - 捏合手势是否可以缩放地图,
 	 		"scrollEnable":Boolean -拖动手势是否可以移动地图,
		}
	</xmp>
 	@returns {void}
 	@description 设置百度地图相关属性
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setRotation
	@function
	@param {Number} args 地图旋转角度，取值范围：-180 - 180
 	@returns {void}
 	@description 设置百度地图旋转角度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setOverlook
	@function
	@param {Number} args 地图俯视角度，取值范围：-45 - 0
 	@returns {void}
 	@description 设置百度地图俯视角度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setScaleBar
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"isShow":Boolean - 是否显示比例尺,
 	 		"position":Object -比例尺的位置，设定坐标以地图左上角为原点,比如:{x:0,y:0},
		}
	</xmp>
 	@returns {void}
 	@description 设置百度地图的比例尺
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setCompass
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"x":Number - 指南针左上角的x坐标
 	 		"y":Number - 指南针左上角的y坐标
		}
	</xmp>
 	@returns {void}
 	@description 设置百度地图指南针位置，只有地图旋转或视角变化时才显示指南针
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setTraffic
	@function
	@param {Boolean} args 是否显示交通状况
 	@returns {void}
 	@description 设置是否显示交通状况
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setHeatMap
	@function
	@param {Boolean} args 是否显示城市热力图
 	@returns {void}
 	@description 设置是否显示城市热力图
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setBuilding
	@function
	@param {Boolean} args 是否显示3D楼块
 	@returns {void}
 	@description 设置是否显示3D楼块效果,地图放大,才会有3D楼快效果,倾斜视角3D效果会更明显
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.setRegion
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"lonDelta":Number - 矩形区域经度差,
 	 		"latDelta":Number - 矩形区域纬度差,
 	 		"center":Object 中心点的经纬度,例如:{lon:116.397, lat:39.910},
 	 		"southwest":Object 左下经纬度，{lon:116.397, lat:39.910},
 	 		"northeast":Object 右上经纬度，{lon:116.397, lat:39.910},
 	 		"animation":Boolean 设置地图的区域时,是否带动画效果,默认为True
		}
	</xmp>
 	@returns {void}
 	@description 设置百度地图显示的矩形区域,可以有两种传入方式，分别为左上右下经纬度/中心点+经纬差，任选其一
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getRegion
	@function
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {Object}返回结构参照setRegion中的参数
 	@description 获取地图显示范围(矩形区域),
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.zoomIn
	@function
 	@returns {void}
 	@description 缩小地图，放大视角，放大一级比例尺，此接口自带动画效果
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.zoomOut
	@function
 	@returns {void}
 	@description 放大地图，缩小视角，缩小一级比例尺，此接口自带动画效果
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.addAnnotations
	@function
	@param {Array} args
	<br/><b>参数格式：</b>
	<xmp>
		[
		{id:1,title:"标题",subTitle:"子标题",lon:20.1,lat:12.3,draggable:true,color:navigator.baiduMap.annotationColor.Red,bgImgPath:"可选参数，背景图片的路径，要求必须是本地路径，不传则显示为默认的大头针"}
		]
	</xmp>
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {void}
 	@description 在地图上添加标注
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.removeAnnotations
	@function
	@param {Array} args
	<br/><b>参数格式：</b>
	<xmp>
		[id1,id2]添加标注时传入的id
	</xmp>
 	@returns {void}
 	@description 在地图上移除标注
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.removeAllAnno
	@function
 	@returns {void}
 	@description 在地图上移除所有标注
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.getAnnotationCoords
	@function
	@param {Number} args 标注的id
	@param {Function} success 成功回调,返回示例:{lon:xx,lat:xx}
 	@param {Function} error 失败回调
 	@returns {void}
 	@description 获取指定标注的经纬度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.updateAnnotationCoords
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
			"id" :Number - 标注的id,
			"lat" :Number - 纬度值,
			"lon" :Number - 经度值
		}
	</xmp>
 	@returns {void}
 	@description 设置某个已添加标注的经纬度
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.annotationExist
	@function
	@param {Number} args 标注的id
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {void}
 	@description 判断某个标注是否存在
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.addLine
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
			"id" :Number - 折线id,
			"points" :Array - 折线的多个点组成的数组,例如:[{lon:116.297,lat:40.109}],
			"styles" :Object - 折线的样式,例如:{borderColor:'#000', borderWidth:3}
		}
	</xmp>
 	@returns {void}
 	@description 在地图上添加折线
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.addPolygon
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
			"id" :Number - 多边形id,
			"points" :Array - 多边形的多个点组成的数组,例如:[{lon:116.297,lat:40.109}],
			"styles" :Object - 多边形的样式,例如:{borderColor:'#000',fillColor: '#000',alpha:0.5, borderWidth:3}
		}
	</xmp>
 	@returns {void}
 	@description 在地图上添加多边形
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.addArc
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
			"id" :Number - 弧形id,
			"points" :Array - 弧形的各个点（弧形两端点和弧形中间点）组成的数组,例如:[{lon:116.297,lat:40.109}],
			"styles" :Object - 弧形的样式,例如:{borderColor:'#000', borderWidth:3}
		}
	</xmp>
 	@returns {void}
 	@description 在地图上添加弧形
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.addCircle
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
			"id" :Number - 圆id,
			"center" :Object - 圆形中心点的经纬度,例如:{lon:116.297,lat:40.109},
			"radius" :Number - 圆的半径,
			"styles" :Object - 圆的样式,例如:{borderColor:'#000', alpha:0.5,fillColor:'#000',borderWidth:3}
		}
	</xmp>
 	@returns {void}
 	@description 在地图上添加圆
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.removeOverlay
	@function
	@param {Number} args 覆盖物的id
 	@returns {void}
 	@description 移除指定id的覆盖物(addLine/addPolygon/addArc/addCircle添加的覆盖物）
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.offLineMapInit
	@function
 	@returns {void}
 	@description 离线地图初始化,使用离线地图之前必须调用此接口
*/

/**
	@name com.justep.cordova.plugin.baiduMapBase.getDistance
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"start":Object - 起点经纬度,例如:{lon:106.486654, lat:29.490295},
 	 		"end":Object - 终点经纬度,
		}
	</xmp>
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {Number} 单位m
 	@description 获取地图两点间的实际距离
*/
/**
	@name com.justep.cordova.plugin.baiduMapBase.transCoords
	@function
	@param {Object} args
	<br/><b>参数格式：</b>
	<xmp>
		{
 	 		"type":String - 原始地理坐标类型,gps(GPS设备采集的原始GPS坐标)/common(google地图,soso地图,aliyun地图,mapabc地图和amap地图所用坐标)
 	 		"lon":Number - 原始地理坐标经度
 	 		"lat":Number - 原始地理坐标纬度
		}
	</xmp>
	@param {Function} success 成功回调
 	@param {Function} error 失败回调
 	@returns {Object} 返回示例:{lon:xx,lat:xx},
 	@description 将其它类型的地理坐标转换为百度坐标。无需调用 open 接口即可使用,
*/





















