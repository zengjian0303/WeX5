//
//  CDVWifiAdmin.h
//  插件开发
//
//  Created by LiangQiangkun on 16/7/13.
//
//
#import <Cordova/CDVPlugin.h>
#import <SystemConfiguration/CaptiveNetwork.h>
@interface CDVWifiAdmin : CDVPlugin
- (void)getWifiInfo:(CDVInvokedUrlCommand *)command;
@end
