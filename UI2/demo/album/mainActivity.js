define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	
	var Model = function(){
		this.callParent();
	};
	
	var picData="";
	Model.prototype.modelLoad = function(event){
		picData=this.comp("picData");
		var jsonStr='{"@type":"table","row":['
		+'{"picSrc":{"value":"$UI/demo/album/images/1.jpg"}},'
		+'{"picSrc":{"value":"$UI/demo/album/images/2.jpg"}},'
		+'{"picSrc":{"value":"$UI/demo/album/images/3.jpg"}},'
		+'{"picSrc":{"value":"$UI/demo/album/images/4.jpg"}}'
		+']}';
		
		var jsonObj=JSON.parse(jsonStr);
		//alert(jsonObj[0].val("picSrc"));
		//picData.loadData(jsonObj,false);
		//picData.saveData();
	};
	
	Model.prototype.getImageUrl = function(row){
		return require.toUrl(row.val('picSrc'));
	};

	Model.prototype.listTemplateUl1Click = function(event){
		var src=picData.getValue("picSrc", picData.getCurrentRow());
		var url = require.toUrl("$UI/demo/album/infoActivity.w?process=/demo/components/process/ui2/ui2Process/&activity=mainActivity&data=" + src);
		justep.Portal.openWindow(url, {title: ""});
	};
		
	return Model;
});