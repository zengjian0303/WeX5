define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	require("$UI/demo/album/js/islider");

	var Model = function(){
		this.callParent();
	};

	Model.prototype.backBtnClick = function(){
		justep.Portal.closeWindow();
	};

	Model.prototype.modelLoad = function(event){
		var picSrc = require.toUrl(this.getContext().getRequestParameter("data"));
		$("#image").attr({"src":picSrc});
		
		var list = [
	        {height:475,width:400,content: require.toUrl("$UI/demo/album/images/1.jpg")}, 
	        {height:475,width:400,content: require.toUrl("$UI/demo/album/images/2.jpg")}, 
	        {height:475,width:400,content: require.toUrl("$UI/demo/album/images/3.jpg")}, 
	        {height:475,width:400,content: require.toUrl("$UI/demo/album/images/4.jpg")}
	    ];	
	    var islider = new iSlider({
	        type: 'pic',
	        data: list,
	        dom: document.getElementById("iSlider-wrapper"),
	        isLooping: true,
	        animateType: 'default',
	        useZoom: true
	    });

	};
    
	return Model;
});